FROM node:20-alpine
RUN apk add --no-cache git
RUN git clone https://github.com/Exp1o1/Chemistry.git
WORKDIR /Chemistry
RUN npm install
CMD npm start